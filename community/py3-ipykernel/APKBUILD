# Contributor: Aiden Grossman <agrossman154@yahoo.com>
# Maintainer: Aiden Grossman <agrossman154@yahoo.com>
pkgname=py3-ipykernel
pkgver=6.16.2
pkgrel=0
pkgdesc="IPython kernel for jupyter"
url="https://github.com/ipython/ipykernel"
arch="noarch"
license="BSD-3-Clause"
depends="
	ipython
	py3-jupyter_client
	py3-matplotlib-inline
	py3-nest_asyncio
	py3-packaging
	py3-psutil
	py3-tornado
	py3-traitlets
	python3
	"
makedepends="py3-build py3-installer py3-hatchling"
checkdepends="py3-ipyparallel py3-pytest py3-flaky py3-pytest-timeout"
source="$pkgname-$pkgver.tar.gz::https://github.com/ipython/ipykernel/releases/download/v$pkgver/ipykernel-$pkgver.tar.gz"
builddir="$srcdir/ipykernel-$pkgver"
options="!check" # py3-ipyparallel is circular, and an optional dep

build() {
	python3 -m build --wheel --no-isolation --skip-dependency-check
}

check() {
	pytest
}

package() {
	python3 -m installer --destdir="$pkgdir" dist/*.whl
}

sha512sums="
3a3bae6cc8ace33b7b8c9d335c7da73c1983824b74b29b77c14cbe24239d556c9c967a759a37a60745823cb6ab698a21262457082464cf33ec1b02ebbe2240d9  py3-ipykernel-6.16.2.tar.gz
"
