# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=glib
pkgver=2.74.1
pkgrel=0
pkgdesc="Common C routines used by Gtk+ and other libs"
url="https://developer.gnome.org/glib/"
arch="all"
license="LGPL-2.1-or-later"
triggers="$pkgname.trigger=/usr/share/glib-2.0/schemas:/usr/lib/gio/modules:/usr/lib/gtk-4.0"
depends_dev="
	bzip2-dev
	docbook-xml
	docbook-xsl
	gettext-dev
	libxml2-utils
	libxslt
	python3
	"
makedepends="$depends_dev pcre2-dev meson zlib-dev libffi-dev util-linux-dev"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-static $pkgname-dev $pkgname-lang"
source="https://download.gnome.org/sources/glib/${pkgver%.*}/glib-$pkgver.tar.xz
	0001-gquark-fix-initialization-with-c-constructors.patch
	deprecated-no-warn.patch
	0001-gslice-remove-slice-allocator.patch
	"
options="!check" # don't like to be run without first being installed

# secfixes:
#   2.66.6-r0:
#     - CVE-2021-27219 GHSL-2021-045
#   2.62.5-r0:
#     - CVE-2020-6750
#   2.60.4-r0:
#     - CVE-2019-12450

build() {
	abuild-meson \
		--default-library=both \
		-Dman=true \
		-Dtests="$(want_check && echo true || echo false)" \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/bin "$subpkgdir"/usr/share
		find "$pkgdir"/usr/bin ! -name "glib-compile-schemas" -a \( \
		-name "gdbus-codegen" -o \
		-name "gobject-query" -o \
		-name "gresource" -o \
		-name "gtester*" -o \
		-name "glib-*" \) \
		-exec mv {} "$subpkgdir"/usr/bin \;

	amove usr/share/gdb usr/share/glib-2.0
}

static() {
	default_static
	depends="gettext-static"
}

sha512sums="
21176cb95fcab49a781d02789bf21191a96a34a6391f066699b3c20b414b3169c958bd86623deb34ca55912083862885f7a7d12b67cc041467da2ba94d9e83c3  glib-2.74.1.tar.xz
32e5aca9a315fb985fafa0b4355e4498c1f877fc1f0b58ad4ac261fb9fbced9f026c7756a5f2af7d61ce756b55c8cd02811bb08df397040e93510056f073756b  0001-gquark-fix-initialization-with-c-constructors.patch
744239ea2afb47e15d5d0214c37d7c798edac53797ca3ac14d515aee4cc3999ef9716ba744c64c40198fb259edc922559f77c9051104a568fc8ee4fc790810b1  deprecated-no-warn.patch
8d468aac6b32c94c73b0a0a2270d3054f85bc101b1f660610336673210df8d65bdc73b5f5866c1e0d5c2bfa31674919de9b19638328fe8a0644dec721bfc1f74  0001-gslice-remove-slice-allocator.patch
"
